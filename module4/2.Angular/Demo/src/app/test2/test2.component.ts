// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-test2',
//   templateUrl: './test2.component.html',
//   styleUrl: './test2.component.css'
// })
// export class Test2Component implements OnInit {
//   person = {
//     id: 1,
//     name: 'John Doe',
//     age: 30,
//     hobbies: ['Reading', 'Traveling', 'Gaming','movies','singing'],
//     address: {
//       streetNo: 111,
//       city: 'Hyd',
//       state: 'Telanagana'
//     }
//   }
//   constructor() { }

//   ngOnInit(): void {
//   }
// }
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test2',
  templateUrl: './test2.component.html',
  styleUrl: './test2.component.css'
})
export class Test2Component implements OnInit {

  person: any;

  constructor() {
    this.person = {
      id: 101,
      name: 'Harsha',
      age: 25,
      hobbies: ['Running', 'Music', 'Movies', 'GYM', 'Reading'],
      address: {streetNo: 111, city: 'Hyd', state: 'Telangana'}
    };
  }
  
  ngOnInit() {
  }
  submit() {
    alert("Id:" + this.person.id + "\n" + "Name: " + this.person.name);
    console.log(this.person);
  }


}

