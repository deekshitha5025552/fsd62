<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Register Page</title>
</head>
<body>

<h1>Register Form</h1>

<form action="RegisterServlet" method="post">

	<table>
	
		<tr>
			<td>Enter studentName</td>
			<td><input type="text" name="studentName" /></td>
		</tr>
		
		<tr>
			<td>Select Gender</td>
			<td>
				<select name="gender">
					<option value="" selected> Select Gender </option>
					<option value="Male" > Male </option>
					<option value="Female" > Female </option>
					<option value="Other"> Others </option>
				</select>
			</td>
		</tr>
		
		<tr>
			<td>Enter Email-Id</td>
			<td><input type="text" name="emailId" /></td>
		</tr>
		
		<tr>
			<td>Enter Password</td>
			<td><input type="password" name="password" /></td>
		</tr>
		
		
		<tr>
			<td></td>
			<td>
				<button>Register</button> Already User? <a href='Login.jsp'>SignIn</a>
			</td>
		</tr>
	</table>	
</form>


</body>
</html>